# Frontend Styles

This repository contains frontend stylesheets in the form of SCSS files which are used in our platform.
Upon request we can add your style customizations to your installation. 

Please also see our [developer documentation](https://docs.parken.arivo.app/developer/custom-css.html).
